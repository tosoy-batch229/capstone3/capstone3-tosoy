import React, { useState, useEffect } from "react";
import "./App.css";
import AppNavbar from "./components/AppNavbar";
import jwt_decode from "jwt-decode";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { Products } from "./components/Products";
import { getData } from "./utils/ApiRequest";
import Cart from "./components/Cart";
import { Container } from "react-bootstrap";
import swal from "sweetalert2";

function App() {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);

  const access = localStorage.getItem("access");
  const user = access ? jwt_decode(access) : undefined;
  const isAdmin = user?.isAdmin;

  useEffect(() => {
    getData(isAdmin ? "products/all" : "products/active", access).then((data) =>
      setProducts(data)
    );
    getData("orders/allOrders", access).then((data) => setCart(data));
  }, [access, isAdmin]);

  return (
    <React.Fragment>
      <AppNavbar />
      <Router>
        <Routes>
          <Route
            path="/products"
            loader={({ params }) => {
              console.log(params.teamId);
            }}
            element={
              <React.Fragment>
                {products.length &&
                  products?.map((product, index) => (
                    <Products product={product} access={access} key={index} />
                  ))}
              </React.Fragment>
            }
          />
          <Route
            path="/cart"
            loader={({ params }) => {
              console.log(params.teamId);
            }}
            element={
              cart.length ? <Cart cart={cart} /> 
              : 
              <Container className="my-5">
                <h2>You have no order </h2>
              </Container>
            }
          />
        </Routes>
      </Router>
    </React.Fragment>
  );
}

export default App;
