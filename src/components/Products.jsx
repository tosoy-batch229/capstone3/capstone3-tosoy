import React, { useState } from "react";
import jwt_decode from "jwt-decode";
import Form from "react-bootstrap/Form";
import { Card, Button, Container } from "react-bootstrap";
import { postData, updateData } from "../utils/ApiRequest";
import { UpdateProductModal } from "./UpdateProductModal";
import swal from "sweetalert2";
import InputGroup from "react-bootstrap/InputGroup";

import { BiEditAlt, BiCheck, BiX } from "react-icons/bi";
import { IoIosPricetags } from "react-icons/io";

export const Products = (props) => {
  const { product, access } = props;
  const { name, author, description, price, isActive, _id } = product;
  const [showUpdateProductModal, setShowUpdateProductModal] = useState(false);

  const [formData, setFormData] = useState(product);

  const user = access ? jwt_decode(access) : undefined;
  const isAdmin = user?.isAdmin;
  const onChange = (inputName, value) => {
    setFormData((prevValues) => {
      return { ...prevValues, [inputName]: value };
    });
  };

  const handleOnSubmit = () => {
    updateData("products/update/", formData, access).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
    });
    setShowUpdateProductModal(false);
  };

  const handleOnDeactivate = () => {
    updateData(
      "products/archive/",
      { ...product, isActive: false },
      access
    ).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
    });
  };

  const handleOnActivate = () => {
    updateData(
      "products/archive/",
      { ...product, isActive: true },
      access
    ).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
    });
  };
  const handleOnAddToCart = () => {
    postData(
      "orders/create/",
      { _id, quantity: formData?.quantity || 1 },
      access
    ).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
      if (data?.message === "Thank You For Buying") {
        swal.fire({
          title: "Thank You For Buying",
          icon: "success",
        });
      }
    });
  };

  return (
    <div>
      <Container>
        <Card className="my-2">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{author}</Card.Text>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Text><IoIosPricetags /> {price}</Card.Text>


            {isAdmin && (
              <Button onClick={() => setShowUpdateProductModal(true)}>
                <BiEditAlt />
              </Button>
            )}
            {isAdmin &&
              (isActive ? (
                <Button className="m-1" onClick={handleOnDeactivate}>
                <BiX />
                </Button>
              ) : (
                <Button className="m-1" onClick={handleOnActivate}>
                <BiCheck />
                </Button>
              ))}

            
            {!isAdmin && (
              <>
              
              <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">Quantity : </InputGroup.Text>
                <Form.Control
                  className="shadow-none"
                  aria-describedby="basic-addon1"
                  type="number"
                  defaultValue={1}
                  onChange={(e) => onChange("quantity", e.target.value)}
                />
              </InputGroup>
              <Button onClick={handleOnAddToCart}>Add to Cart</Button>
              </>
            )}
          </Card.Body>
        </Card>
        <UpdateProductModal
          showUpdateProductModal={showUpdateProductModal}
          handleOnSubmit={handleOnSubmit}
          onChange={onChange}
          setFormData={setFormData}
          setShowUpdateProductModal={setShowUpdateProductModal}
          product={product}
        />
      </Container>
    </div>
  );
};
