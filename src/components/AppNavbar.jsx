import React, { useState } from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import { postData } from "../utils/ApiRequest";
import jwt_decode from "jwt-decode";
import swal from "sweetalert2";
import { RegisterModal } from "./RegisterModal";
import { LoginModal } from "./LoginModal";
import { CreateProductModal } from "./CreateProductModal";

import { BiShoppingBag, BiInfoCircle, BiPhoneCall, BiPlusCircle, BiCartAlt } from "react-icons/bi";
import { SlLogin, SlLogout } from "react-icons/sl";
import { FaBookReader } from 'react-icons/fa' 
import { IoMdCreate } from 'react-icons/io' 

const AppNavbar = () => {
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [showRegisterForm, setShowRegisterForm] = useState(false);
  const [showCreateProductForm, setShowCreateProductForm] = useState(false);

  const access = localStorage.getItem("access");
  const user = access ? jwt_decode(access) : undefined;

  const [formData, setFormData] = useState({});

  const onChange = (inputName, value) => {
    setFormData((prevValues) => {
      return { ...prevValues, [inputName]: value };
    });
  };

  const { username, email, password, name, author, description, price } =
    formData;
  const handleRegister = (e) => {
    e.preventDefault();
    postData("users/register/", {
      username: username,
      email: email,
      password: password,
      isAdmin: false,
    }).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
      if (data?.Message === "User created successfully") {
        swal.fire({
          title: "User created successfully",
          icon: "success",
          text: "Welcome to Tome Reader!",
        });
      }
    });
    setShowRegisterForm(false);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    postData("users/userAuth/", {
      username: username,
      password: password,
    }).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
      if (data?.access) {
        localStorage.setItem("access", data.access);

        swal.fire({
          title: "Login Successfull",
          icon: "success",
          text: "Welcome to Tome Reader!",
        });
      }
    });
    setShowLoginForm(false);
  };

  const handleLogOut = () => {
    localStorage.clear();
  };

  const handleCreateProduct = () => {
    postData(
      "products/create/",
      {
        name,
        author,
        description,
        price,
      },
      access
    ).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
    });
    setShowCreateProductForm(false);
  };

  return (
    <>
      <Navbar
        expand="lg"
        bg="dark"
        variant="dark"
        collapseOnSelect
        className="py-3"
      >
        <Container>
          <Navbar.Brand href="/" className="text-light">
            <FaBookReader className="mb-2 mx-1"/>
            Tome Reader
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="responsive-navbar-nav"
            className="shadow-none"
          />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto">
              <Nav.Link href="/products" className="text-light">
                <BiShoppingBag className="mb-1 mx-1" />
                Products
              </Nav.Link>
              <Nav.Link href="/about" className="text-light">
                <BiInfoCircle className="mb-1 mx-1" />
                About
              </Nav.Link>
              <Nav.Link href="/contact-us" className="text-light">
                <BiPhoneCall className="mb-1 mx-1"/>
                Contact Us
              </Nav.Link>
            </Nav>
            <Nav>
              {user?.isAdmin && (
                <Nav.Link>
                  <Button
                    variant="primary"
                    onClick={() => {
                      setShowCreateProductForm(true);
                    }}
                  >
                    <BiPlusCircle className="mb-1 mx-1"/>
                  </Button>
                </Nav.Link>
              )}
              {user ? (
                <Nav.Link href="/cart">
                  <Button variant="primary">
                  <BiCartAlt className="mb-1 mx-1"/>
                  </Button>
                </Nav.Link>
              ) : (
                <Nav.Link>
                  <Button
                    variant="primary"
                    onClick={() => {
                      setShowLoginForm(true);
                    }}
                  >
                    <SlLogin className="mb-1 mx-1"/>
                    Login
                  </Button>
                </Nav.Link>
              )}
              {user ? (
                <Nav.Link>
                  <Button variant="primary" onClick={handleLogOut}>
                    <SlLogout className="mb-1 mx-1"/>
                    Log out
                  </Button>
                </Nav.Link>
              ) : (
                <Nav.Link>
                  <Button
                    variant="primary"
                    onClick={() => {
                      setShowRegisterForm(true);
                    }}
                  >
                    <IoMdCreate className="mb-1 mx-1"/>
                    Register
                  </Button>
                </Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <LoginModal
        showLoginForm={showLoginForm}
        handleLogin={handleLogin}
        onChange={onChange}
        setFormData={setFormData}
        setShowLoginForm={setShowLoginForm}
      />

      <RegisterModal
        showRegisterForm={showRegisterForm}
        handleRegister={handleRegister}
        onChange={onChange}
        setFormData={setFormData}
        setShowRegisterForm={setShowRegisterForm}
      />

      <CreateProductModal
        showCreateProductForm={showCreateProductForm}
        handleCreateProduct={handleCreateProduct}
        onChange={onChange}
        setFormData={setFormData}
        setShowCreateProductForm={setShowCreateProductForm}
      />
    </>
  );
};

export default AppNavbar;
