import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
export const LoginModal = (props) => {
  const {
    showLoginForm,
    handleLogin,
    onChange,
    setFormData,
    setShowLoginForm,
  } = props;
  return (
    <Modal show={showLoginForm}>
      <Modal.Header closeButton>
        <Modal.Title>Login User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleLogin}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              onChange={(e) => onChange("username", e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => onChange("password", e.target.value)}
            />
          </Form.Group>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => {
                setFormData({});
                setShowLoginForm(false);
              }}
            >
              Close
            </Button>
            <Button variant="primary" type="submit">
              Log In
            </Button>
          </Modal.Footer>
        </Form>
      </Modal.Body>
    </Modal>
  );
};
