import React from "react";
import { Card, Container } from "react-bootstrap";

const Cart = (props) => {
  const { cart } = props;

  return (
    <div>
      <Container>
        {cart?.map((cart) => {
          return (
            <Card className="my-2">
              <Card.Body>
                <Card.Title>
                  Product Name: {cart?.products[0].productName}
                </Card.Title>
                <Card.Text>Quantity: {cart?.products[0].quantity}</Card.Text>

                <Card.Text>Price: {cart?.totalAmount}</Card.Text>
              </Card.Body>
            </Card>
          );
        })}
        ;
      </Container>
    </div>
  );
};

export default Cart;
