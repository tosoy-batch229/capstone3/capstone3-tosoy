import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";



export const UpdateProductModal = (props) => {
  const {
    showUpdateProductModal,
    handleOnSubmit,
    onChange,
    setFormData,
    setShowUpdateProductModal,
    product,
  } = props;
  const { name, author, description, price } = product;
  return (
    <Modal show={showUpdateProductModal}>
      <Modal.Header closeButton>
        <Modal.Title>Create Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleOnSubmit}>
          <Form.Group className="mb-3" controlId="formBasicName">
            <Form.Label>
              Name
            </Form.Label>
            <Form.Control
              className="shadow-none"
              type="text"
              placeholder="Enter name"
              onChange={(e) => onChange("name", e.target.value)}
              defaultValue={name}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicAuthor">
            <Form.Label>Author</Form.Label>
            <Form.Control
              className="shadow-none"
              type="text"
              placeholder="Enter author"
              onChange={(e) => onChange("author", e.target.value)}
              defaultValue={author}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control
              className="shadow-none"
              as="textarea"
              rows={3}
              onChange={(e) => onChange("description", e.target.value)}
              defaultValue={description}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control
              className="shadow-none"
              type="number"
              placeholder="Enter price"
              onChange={(e) => onChange("price", e.target.value)}
              defaultValue={price}
            />
          </Form.Group>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => {
                setFormData({});
                setShowUpdateProductModal(false);
              }}
            >
              Close
            </Button>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal.Body>
    </Modal>
  );
};
