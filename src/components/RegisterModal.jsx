import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

import { CgProfile } from 'react-icons/cg' 
import { SiMinutemailer } from 'react-icons/si' 
import { RiLockPasswordLine } from 'react-icons/ri' 


export const RegisterModal = (props) => {
  const {
    showRegisterForm,
    handleRegister,
    onChange,
    setFormData,
    setShowRegisterForm,
  } = props;
  return (
    <Modal show={showRegisterForm}>
      <Modal.Header closeButton>
        <Modal.Title>Register User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleRegister}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>
              <CgProfile className="mb-1 mx-1"/> 
              Username
            </Form.Label>
            <Form.Control
              className="shadow-none"
              type="text"
              placeholder="Enter username"
              onChange={(e) => onChange("username", e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>
              <SiMinutemailer className="mb-1 mx-1"/> 
              Email address
            </Form.Label>
            <Form.Control
              className="shadow-none"
              type="email"
              placeholder="Enter email"
              onChange={(e) => onChange("email", e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>
              <RiLockPasswordLine className="mb-1 mx-1"/> 
              Password
            </Form.Label>
            <Form.Control
              className="shadow-none"
              type="password"
              placeholder="Password"
              onChange={(e) => onChange("password", e.target.value)}
            />
          </Form.Group>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => {
                setFormData({});
                setShowRegisterForm(false);
              }}
            >
              Close
            </Button>
            <Button variant="primary" type="submit">
              Register
            </Button>
          </Modal.Footer>
        </Form>
      </Modal.Body>
    </Modal>
  );
};
